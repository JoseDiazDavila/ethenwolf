<?php

use Illuminate\Support\Facades\Route;
use App\Product;
use App\Merchandising;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;


/* ------- Bindeos ------- */
/* product inyeccion de dependencias */
Route::bind('product', function($name) {
    return App\Product::where('name', $name)->first();
});

/* category dependency injection */
Route::bind('category', function($category) {
    return App\Category::find($category);
});

/* user inyeccion de dependencias */
Route::bind('user', function($user) {
    return App\User::where('user', $user)->first();
});

/* merch inyeccion de dependencias */
Route::bind('merchandising', function($name) {
    return App\Merchandising::where('name', $name)->first();
});

/* -- mail -- */
Route::post('contactanos', 'StoreController@email');

/* ----------- Rutas principales ---------- */
/* ruta q va a inicio , as sirve como alias y uses el controlador que usa */
Route::get('/', ['as' => 'inicio', 'uses' => 'StoreController@index']);

/* ruta q va a asociaciones */
Route::get('/asociaciones', ['as' => 'asociaciones', 'uses' => 'StoreController@asociaciones']);

/* ruta q va a conciertos */
Route::get('/conciertos', ['as' => 'conciertos', 'uses' => 'StoreController@conciertos']);

/* ruta q va a contactanos */
Route::get('/contactanos', ['as' => 'contactanos', 'uses' => 'StoreController@contactanos']);

/* ruta q va a quienes somos */
Route::get('/quienesSomos', ['as' => 'quienesSomos', 'uses' => 'StoreController@quienesSomos']);

/* ruta q va a productos */
Route::get('/products', ['as' => 'producto', 'uses' => 'StoreController@product']);

/* ruta que va a producto detalle */
Route::get('products/{name}', ['as' => 'detail', 'uses' => 'StoreController@show']);

/* ruta q va a merchandising */
Route::get('/merchandising', ['as' => 'merchandising', 'uses' => 'StoreController@merchandising']);

/* ruta que va a producto merchandising */
Route::get('merchandising/{name}', ['as' => 'detail-merch', 'uses' => 'StoreController@show_merch']);


/* ------ Rutas Carrito------- */
/* ruta carrito */
Route::get('cart/show', ['as' => 'cart-show', 'uses' => 'CartController@show']);

/* agregar producto carrito */
Route::get('cart/add/{product}', ['as' => 'cart-add', 'uses' => 'CartController@add']);

/* peticion borrar del carrito */
Route::get('cart/delete/{product}', ['as' => 'cart-delete', 'uses' => 'CartController@delete']);

/* cantidad de productos */
Route::get('cart/update/{product}/{quantity}', ['as' => 'cart-update', 'uses' => 'CartController@update']);

/* agregar merchandising carrito */
Route::get('cart/add_merch/{merchandising}', ['as' => 'cart-add-merch', 'uses' => 'CartController@add_merch']);

/* peticion borrar merchandising del carrito */
Route::get('cart/delete_merch/{merchandising}', ['as' => 'cart-delete', 'uses' => 'CartController@delete_merch']);

/* cantidad de productos */
Route::get('cart/update_merch/{merchandising}/{quantity}', ['as' => 'cart-update', 'uses' => 'CartController@update_merch']);

/* peticion vaciar el carrito */
Route::get("cart/trash/", ['as' => 'cart-trash', 'uses' => 'CartController@trash']);



/* ----------- todas las rutas de auth --------- */
Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');


/* ruta para crear nuevo usuario de registro */
Route::post('user/create', array('as' => 'user.create', 'uses' => 'Auth\RegisterController@create'));

/* ruta ver detalle del pedido pasando por middeleware para iniciar sesion antes de comprar */
Route::get('order-detail', ['middleware' => 'auth', 'as' => 'order-detail', 'uses' => 'CartController@orderDetail']);


/* ---------- Rutas de PayPal ------------ */
/* peticion Paypal */
Route::get('payment', array('as' => 'payment', 'uses' => 'PaypalController@postPayment'));

/* respuesta paypal */
Route::get('payment/status', array('as' => 'payment.status', 'uses' => 'PaypalController@getPaymentStatus'));


/* ---------Panel administrador--------------- */
Route::group(['namespace' => 'Admin', 'middleware' => ['auth'], 'prefix' => 'admin'], function() {

    /* ----------- administrador home ------------ */
    Route::get('home', function () {
        return view('admin.home');
    });

    /* ---------- CATEGORIAS --------------- */
    /* ruta index de administrador categoria */
    Route::resource('category', 'CategoryController');

    /* ruta categoria index */
    Route::get('category', array('as' => 'admin.category.index', 'uses' => 'CategoryController@index'));

    /* ruta create categoria */
    Route::get('category/create', ['as' => 'admin.category.create', 'uses' => 'CategoryController@create']);

    /* ruta categoria store */
    Route::post('category/store', array('as' => 'admin.category.store', 'uses' => 'CategoryController@store'));

    /* ruta categoria editar */
    Route::get('category/edit/{category?}', array('as' => 'admin.category.edit', 'uses' => 'CategoryController@edit'));

    /* ruta categoria update */
    Route::post('category/update/{category?}', array('as' => 'admin.category.update', 'uses' => 'CategoryController@update'));

    /* ruta categoria borrar */
    Route::delete('category/destroy/{category?}', array('as' => 'admin.category.destroy', 'uses' => 'CategoryController@destroy'));


    /* ------------- PRODUCTOS -------------- */
    /* ruta index de administrador categoria */
    Route::resource('product', 'ProductController');

    /* ruta index producto */
    Route::get('product', array('as' => 'admin.product.index', 'uses' => 'ProductController@index'));

    /* ruta create  producto */
    Route::get('product/create', array('as' => 'admin.product.create', 'uses' => 'ProductController@create'));

    /* ruta store producto */
    Route::post('product/store', array('as' => 'admin.product.store', 'uses' => 'ProductController@store'));

    /* ruta editar producto */
    Route::get('product/edit/{product?}', array('as' => 'admin.product.edit', 'uses' => 'ProductController@edit'));

    /* ruta update producto */
    Route::post('product/update/{product?}', array('as' => 'admin.product.update', 'uses' => 'ProductController@update'));

    /* ruta borrar producto */
    Route::delete('product/destroy/{product?}', array('as' => 'admin.product.destroy', 'uses' => 'ProductController@destroy'));


    /* ------------- MERCHANDISING -------------- */
    /* ruta index de administrador categoria */
    Route::resource('merchandising', 'MerchandisingController');

    /* ruta index producto */
    Route::get('merchandising', array('as' => 'admin.merchandising.index', 'uses' => 'MerchandisingController@index'));

    /* ruta create  producto */
    Route::get('merchandising/create', array('as' => 'admin.merchandising.create', 'uses' => 'MerchandisingController@create'));

    /* ruta store producto */
    Route::post('merchandising/store', array('as' => 'admin.merchandising.store', 'uses' => 'MerchandisingController@store'));

    /* ruta editar producto */
    Route::get('merchandising/edit/{merchandising?}', array('as' => 'admin.merchandising.edit', 'uses' => 'MerchandisingController@edit'));

    /* ruta update producto */
    Route::post('merchandising/update/{merchandising?}', array('as' => 'admin.merchandising.update', 'uses' => 'MerchandisingController@update'));

    /* ruta borrar producto */
    Route::delete('merchandising/destroy/{merchandising?}', array('as' => 'admin.merchandising.destroy', 'uses' => 'MerchandisingController@destroy'));




    /* ------------- Usuarios --------------- */
    /* ruta index de administrador usuario */
    Route::resource('user', 'UserController');

    /* ruta index de usuarios  */
    Route::get('user', array('as' => 'admin.user.index', 'uses' => 'UserController@index'));

    /* ruta create de usuarios  */
    Route::get('user/create', array('as' => 'admin.user.create', 'uses' => 'UserController@create'));

    /* ruta store de usuarios  */
    Route::post('user/store', array('as' => 'admin.user.store', 'uses' => 'UserController@store'));

    /* ruta editar de usuarios  */
    Route::get('user/edit/{user?}', array('as' => 'admin.user.edit', 'uses' => 'UserController@edit'));

    /* ruta update de usuarios  */
    Route::post('user/update/{user?}', array('as' => 'admin.user.update', 'uses' => 'UserController@update'));

    /* ruta borrar de usuarios  */
    Route::delete('user/destroy/{user?}', array('as' => 'admin.user.destroy', 'uses' => 'UserController@destroy'));


    /* ---------------- Pedidos --------------- */
    /* ruta index de administrador pedidos */
    Route::resource('orders', 'UserController');

    /* ruta index de pedidos */
    Route::get('order', array('as' => 'admin.order.index', 'uses' => 'OrderController@index'));

    /* ruta  */
    Route::post('order/getItems', array('as' => 'admin.order.getItems', 'uses' => 'OrderController@getItems'));

    /* ruta borrar pedidos */
    Route::delete('order/destroy/{order?}', array('as' => 'admin.order.destroy', 'uses' => 'OrderController@destroy'));
});
