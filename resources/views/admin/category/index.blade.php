@extends('admin.template')
@section('content')


<div style="background: #3B3A3A"><br>

    <h2 style="text-align: center"> - ETHENWOLF -  </h2>
    <h2 style="text-align: center"> - Panel de Categorias - </h2>

    <hr class="featurette-divider">

    <div class="container">
    <div class="table-responsive" >
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr style="color: white">

                    <!-- Boton editar categoria-->
                    <th>Editar Categoria</th>

                    <!-- Boton eliminar categoria-->
                    <th>Eliminar Categoria</th>

                    <th>Nombre de Categoria</th>
                    <th>Descripcion Categoria</th>
                </tr>
            </thead>

            <!-- Inicio bucle para recorrer las categorias-->
            <tbody>
                @foreach($categories as $category)

            <td> <a href="{{ route('admin.category.edit', $category) }}" class="btn btn-primary"><i class="fa fa-pencil-square"></i></a></td>

            <td>
                <form method="post" action="{{route('admin.category.destroy',$category)}}">
                    @csrf <!-- {{ csrf_field() }} -->
                    <input type="hidden" name="_method" value="DELETE">
                    <button onclick="return confirm('Eliminar registro?')" class="btn btn-danger">
                        <i class="fa fa-trash"></i>

                    </button>
                </form>
            </td>



            <td style="color: white"> {{ $category->name }}</td>
            <td style="color: white"> {{ $category->description }}</td>

            </tbody>
            @endforeach
        </table>

        <!-- Boton Nueva Categoria-->
        <div align="center">
            <a href="{{ route('admin.category.create') }}" class="btn btn-warning"><i class="fa fa-plus-circle"></i>Añadir Nueva Categoria</a>
        </div>
    </div><br>
</div>
</div>
@stop
