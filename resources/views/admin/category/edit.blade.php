@extends('admin.template')
@section('content')

<div style="background: #3B3A3A"><br>

    <h2 style="text-align: center"> - Editar Categoria - </h2>
    <hr class="featurette-divider">

    <br><div class="table-responsive col-md-6 " style="width: 80%; margin:0 auto; background: white; border-radius: 3em; margin-bottom:  30px; text-align: center; color: black">

        @if(count($errors)> 0)
        @include('admin.partials.errors')
        @endif

        <!-- Formulario -->
        <form role="form" method="post" action="{{ route('admin.category.update', $category) }}">
            <div class="form-group"><br>

                @csrf <!-- {{ csrf_field() }} -->

                <label for="name">Nombre:</label>
                <input type="text" name="name" class="form-control" placeholder="ingresa el nombre..." autofocus>

                <label for="description">Descripcion</label>
                <input type="text" name="description" class="form-control"><br>

                <div>
                    <input type="submit" class="btn btn-primary" value="Actualizar">

                    <a href="{{route('admin.category.index')}}" class="btn btn-warning"> Cancelar</a>
                </div>
            </div>
        </form>
    </div><br>
    @stop
</div>







