
<!-- Navegador-->
<div style="background: black">
    <div  class="container" >

        <nav class="navbar navbar-expand-lg  navbar-light justify-content-md-center " style="background: black " >

            <li class="dropdown" style="list-style: none" >

                <!-- -->
                <a href="#" class="dropdown-toggle nav-item btn btn-border btn-danger" data-toggle="dropdown" role="button" aria-expanded="false">
                    <i class="fa fa-user fa-2x"></i>{{ Auth::user()->user }}<span class="caret"></span>
                </a>

                <!-- Finalizar sesion-->
                <ul class="dropdown-menu" role="menu">
                    <a class="dropdown-item" href="{{ route('logout') }}">Finalizar Sesión</a>
                </ul>

                <a type="button" class="nav-item btn btn-border" href="{{route('admin.category.index')}}">Categorías</a>

                <a type="button" class="nav-item btn btn-border" href="{{route('admin.product.index')}}">Canciones</a>

                <a type="button" class="nav-item btn btn-border" href="{{ route('admin.merchandising.index') }}">Merchandising</a>

                <a type="button" class="nav-item btn btn-border" href="{{route('admin.order.index')}}">Pedidos</a>

                <a type="button" class="nav-item btn btn-border"href="{{route('admin.user.index')}}">Usuarios</a>

            </li>

        </nav>
    </div>
</div>
