@extends('admin.template')
@section('content')


<div style="background: #3B3A3A"><br>

    <h2 style="text-align: center"> - ETHENWOLF -  </h2>
    <h2 style="text-align: center"> - Panel de Usuarios - </h2>

    <hr class="featurette-divider">

    <div class="table-responsive" >
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr style="color: white">

                    <!-- Boton editar categoria-->
                    <th>Editar Usuario</th>

                    <!-- Boton eliminar categoria-->
                    <th>Eliminar Usuario</th>

                    <th>Nombre de Usuario</th>
                    <th>Apellido Usuario</th>
                    <th>Correo</th>
                    <th>Nick</th>
                    <th>Contraseña</th>
                    <th>Tipo Usuario</th>
                    <th>Activo</th>
                    <th>Direccion Usuario</th>

                </tr>
            </thead>

            <!-- Inicio bucle para recorrer las categorias-->
            <tbody>
                @foreach($users as $user)

            <td> <a href="{{ route('admin.user.edit', $user->user) }}" class="btn btn-primary"><i class="fa fa-pencil-square"></i></a></td>

            <td>
                <form method="post" action="{{route('admin.user.destroy',$user->user)}}">
                    @csrf <!-- {{ csrf_field() }} -->
                    <input type="hidden" name="_method" value="DELETE">
                    <button onclick="return confirm('Eliminar registro?')" class="btn btn-danger">
                        <i class="fa fa-trash"></i>

                    </button>
                </form>
            </td>

            <td style="color: white"> {{ $user->name }}</td>
            <td style="color: white"> {{ $user->last_name }}</td>
            <td style="color: white"> {{ $user->email }}</td>
            <td style="color: white"> {{ $user->user }}</td>
            <td style="color: white"> {{ $user->password }}</td>
            <td style="color: white"> {{ $user->type }}</td>
            <td style="color: white"> {{ $user->active == 1 ? "Si" : "No" }}</td>
            <td style="color: white"> {{ $user->address }}</td>

            </tbody>
            @endforeach
        </table>

        <!-- Boton Nueva Categoria-->
        <div align="center">
            <a href="{{ route('admin.user.create') }}" class="btn btn-warning"><i class="fa fa-plus-circle"></i>Añadir Nuevo Usuario</a>
        </div>
    </div><br>

    <div>
        <?php echo $users->render(); ?>
    </div><br>

</div>
@stop
