@extends('admin.template')
@section('content')

<div style="background: #3B3A3A"><br>

    <h2 style="text-align: center"> - Editar Usuario - </h2>
    <hr class="featurette-divider">

    <br><div class="table-responsive col-md-6 " style="width: 80%; margin:0 auto; background: white; border-radius: 3em; margin-bottom:  30px; text-align: center; color: black">

        @if(count($errors)> 0)
        @include('admin.partials.errors')
        @endif

        <!-- Formulario -->
        <form role="form" method="post" action="{{ route('admin.user.update', $user->user) }}">
            <div class="form-group"><br>

                @csrf <!-- {{ csrf_field() }} -->

                <label for="name">Nombre:</label>
                <input type="text" name="name" class="form-control" placeholder="ingresa el nombre..." autofocus>

                <label for="last_name">Apellido</label>
                <input type="text" name="last_name" class="form-control"><br>

                <label for="email">Correo Electronico</label>
                <input type="text" name="email" class="form-control"><br>

                <label for="user">Nick</label>
                <input type="text" name="user" class="form-control"><br>

                <label for="password">Contraseña</label>
                <input type="password" name="password" class="form-control"><br>

                <label for="password_confirmation">Confirmar contraseña</label>
                <input type="text" name="password_confirmation" class="form-control">

                <label for="type">Tipo</label>
                <select name="type" class="form-control">

                    <option value="user">user</option>
                    <option value="admin">admin</option>

                </select>

                <label for="active">Activo</label>
                <input type="checkbox" name="active" class="form-control"><br>

                <label for="address">Direccion</label>
                <input type="textarea" name="address" class="form-control"><br>


                <div>
                    <input type="submit" class="btn btn-primary" value="Actualizar">

                    <a href="{{route('admin.user.index')}}" class="btn btn-warning"> Cancelar</a>
                </div>
            </div>
        </form>
    </div><br>
    @stop
</div>
