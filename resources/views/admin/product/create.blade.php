@extends('admin.template')
@section('content')

<div style="background: #3B3A3A"><br>

    <h2 style="text-align: center"> - Agregar Producto - </h2>
    <hr class="featurette-divider">

    <br><div class="table-responsive col-md-6 " style="width: 80%; margin:0 auto; background: white; border-radius: 3em; margin-bottom:  30px; text-align: center; color: black">

        @if(count($errors)> 0)
        @include('admin.partials.errors')
        @endif

        <!-- Formulario -->
        <form role="form" method="post" action="{{ route('admin.product.store') }}">
            <div class="form-group"><br>

                @csrf <!-- {{ csrf_field() }} -->


                <label for="name">Nombre:</label>
                <input type="text" name="name" class="form-control" placeholder="ingresa el nombre..." autofocus required>

                <label for="description">Descripcion</label>
                <input type="text" name="description" class="form-control" required><br>


                <label for="extract">Extract</label>
                <input type="text" name="extract" class="form-control" required><br>


                <label for="price">precio</label>
                <input type="text" name="price" class="form-control" required><br>


                <label for="category_id">Categoria</label>
                <select name="category_id" class="form-control" required>
                    @foreach($categories as $category)
                    <option value="{{key($categories)}}">{{$category}}</option>
                    @endforeach
                </select>


                <label for="visible">Visible</label>
                <input type="checkbox" name="visible" class="form-control" required><br>


                     <label for="audio">Audio</label>
                <input type="text" name="audio" class="form-control" required><br>
                
                
                
                <label for="image">Imagen</label>
                <input type="text" name="image" class="form-control" required><br>


                <div>
                    <input type="submit" class="btn btn-primary" value="Guardar">

                    <a href="{{route('admin.product.index')}}" class="btn btn-warning"> Cancelar</a>
                </div>


            </div>
        </form>
    </div><br>
    @stop
</div>







