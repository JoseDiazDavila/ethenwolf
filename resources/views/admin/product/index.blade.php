@extends('admin.template')
@section('content')

<div style="background: #3B3A3A"><br>

    <h2 style="text-align: center"> - ETHENWOLF -  </h2>
    <h2 style="text-align: center"> - Panel de Canciones - </h2>

    <hr class="featurette-divider">

    <div class="table-responsive" >
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr style="color: white">

                    <!-- Boton editar categoria-->
                    <th>Editar producto</th>

                    <!-- Boton eliminar categoria-->
                    <th>Eliminar Producto</th>

                    <th>Imagen</th>
                    <th>Nombre de Canción</th>
                    <th>Categoria Canción</th>
                    <th>Descripcion Canción</th>
                    <th>Audio</th>
                    <th>Precio</th>
                    <th>Visible</th>
                </tr>
            </thead>

            <!-- Inicio bucle para recorrer las categorias-->
            <tbody>
                @foreach($products as $product)

            <td> <a href="{{route('admin.product.edit', $product->name)}}" class="btn btn-primary"><i class="fa fa-pencil-square"></i></a></td>

            <td>
                <form method="post" action="{{route('admin.product.destroy',$product->name)}}">
                    @csrf <!-- {{ csrf_field() }} -->
                    <input type="hidden" name="_method" value="DELETE">
                    <button onclick="return confirm('Eliminar registro?')" class="btn btn-danger">
                        <i class="fa fa-trash"></i>

                    </button>
                </form>
            </td>

            <td><img src="{{ $product->image }}" width="40"></td>
            <td style="color: white"> {{ $product->name }}</td>
            <td style="color: white"> {{ $product->category->name }} </td>
            <td style="color: white"> {{ $product->description }}</td>
            <td style="color: white"> {{ $product->audio }}</td>
            <td style="color: white"> {{ number_format($product->price,2) }}</td>
            <td style="color: white"> {{ $product->visible == 1 ?"SI" : "NO" }}</td>
            </tbody>
            @endforeach
        </table>

        <!-- Boton Nueva Categoria-->
        <div align="center">
            <a href="{{ route('admin.product.create') }}" class="btn btn-warning"><i class="fa fa-plus-circle"></i>Añadir Nueva Canción</a>
        </div>
    </div>

    <div>
        <?php echo $products->render(); ?>
    </div><br>

</div>

@stop
