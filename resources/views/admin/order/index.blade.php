@extends('admin.template')
@section('content')

<div style="background: #3B3A3A"><br>

    <h2 style="text-align: center"> - ETHENWOLF -  </h2>
    <h2 style="text-align: center"> - Panel de Pedidos - </h2>

    <hr class="featurette-divider">


    <div class="table-responsive" >
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr style="color: white">

                    <th>Ver Detalle Pedido</th>
                    <th>Eliminar Pedido</th>

                    <th>Fecha de pedido</th>
                    <th>Nombre Completo de Usuario</th>
                    
                    <th>Subtotal</th> 
                    <th>Coste de Envio</th> 
                    <th>Precio Total</th> 

                </tr>
            </thead>

            <!-- Inicio bucle para recorrer las categorias-->
            <tbody>
                @foreach($orders as $order)

            <td align="center">
                <a href="#" class="btn btn-primary btn-detalle-pedido" data-id="{{ $order->id }}" data-path="{{ route('admin.order.getItems') }}" data-toggle="modal"
                   data-target="#myModal"data-token="{{ csrf_token() }}"> <i class="fa fa-external-link"></i> </a>
            </td>


            <td align="center">
                <form method="post" action="{{route('admin.order.destroy',$order)}}">
                    @csrf <!-- {{ csrf_field() }} -->
                    <input type="hidden" name="_method" value="DELETE">
                    <button onclick="return confirm('Eliminar registro?')" class="btn btn-danger">
                        <i class="fa fa-trash"></i>

                    </button>
                </form>
            </td>


            <td style="color: white">{{ $order->created_at }}</td>
            <td style="color: white">{{ $order->user['name'] . " " . $order->user['last_name'] }}</td>

            <td style="color: white">{{ number_format($order->subtotal,2) }} €</td>
            <td style="color: white">{{ number_format($order->shipping,2) }} €</td>
            <td style="color: white">{{ number_format($order->subtotal + $order->shipping,2) }} €</td>

            </tbody>
            @endforeach
        </table>

    </div><br>
    <div>
        <?php echo $orders->render(); ?>
    </div><br>

</div>

@include('admin.partials.modal-detalle-pedido')
@stop
