@extends('admin.template')
@section('content')


<div style="background: #3B3A3A"><br>

    <h2 style="text-align: center"> - ETHENWOLF -  </h2>
    <h2 style="text-align: center"> - Panel de Administrador - </h2>

    <hr class="featurette-divider">

    <br><div class="table-responsive col-md-8 " style="width: 80%; margin:0 auto; background: white; border-radius: 3em; margin-bottom:  30px; text-align: center; color: black">

        <div class="row">
            <div class="col-md-8" style="float: none; margin-left: auto; margin-right: auto"><br>
                <div class="panel">
                    <i class="fa fa-list-alt icon-home fa-4x"></i>
                    <a href="{{route('admin.category.index')}}" class="btn  btn-block btn-home-admin miBotonAdmin">CATEGORIAS</a>
                </div><br>
            </div>

            <div class="col-md-6"><br>
                <div class="panel">
                    <i class="fa fa-music icon-home fa-4x"></i>
                    <a href="{{ route('admin.product.index') }}" class="btn  btn-block btn-home-admin miBotonAdmin">CANCIONES</a>
                </div><br>
            </div>

            <div class="col-md-6"><br>
                <div class="panel">
                    <i class="fa fa-shopping-cart icon-home fa-4x"></i>
                    <a href="{{ route('admin.merchandising.index') }}" class="btn  btn-block btn-home-admin miBotonAdmin">MERCHANDISING</a>
                </div><br>
            </div>

            <div class="col-md-6"><br>
                <div class="panel">
                    <i class="fa fa-cc-paypal icon-home fa-4x"></i>
                    <a href="{{ route('admin.order.index') }}" class="btn  btn-block btn-home-admin miBotonAdmin">PEDIDOS</a>
                </div><br>
            </div>

            <div class="col-md-6"><br>
                <div class="panel">
                    <i class="fa fa-users icon-home fa-4x"></i>
                    <a href="{{ route('admin.user.index') }}" class="btn  btn-block btn-home-admin miBotonAdmin">USUARIOS</a>
                </div><br>
            </div>
        </div><br>
    </div><br>
</div>
@stop