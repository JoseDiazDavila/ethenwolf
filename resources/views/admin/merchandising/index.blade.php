@extends('admin.template')
@section('content')

<div style="background: #3B3A3A"><br>

    <h2 style="text-align: center"> - ETHENWOLF -  </h2>
    <h2 style="text-align: center"> - Panel de Merchandising - </h2>

    <hr class="featurette-divider">

    <div class="table-responsive" >
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr style="color: white">

                    <!-- Boton editar categoria-->
                    <th>Editar Merchandising</th>

                    <!-- Boton eliminar categoria-->
                    <th>Eliminar Merchandising</th>

                    <th>Imagen</th>
                    <th>Nombre de Merchandising</th>
                    <th>Categoria Merchandising</th>
                    <th>Descripcion Merchandising</th>
                    <th>Precio</th>
                    <th>Visible</th>
                </tr>
            </thead>

            <!-- Inicio bucle para recorrer las categorias-->
            <tbody>
                @foreach($merchandising as $merchandising)

            <td> <a href="{{route('admin.merchandising.edit', $merchandising->name)}}" class="btn btn-primary"><i class="fa fa-pencil-square"></i></a></td>

            <td>
                <form method="post" action="{{route('admin.merchandising.destroy',$merchandising->name)}}">
                    @csrf <!-- {{ csrf_field() }} -->
                    <input type="hidden" name="_method" value="DELETE">
                    <button onclick="return confirm('Eliminar registro?')" class="btn btn-danger">
                        <i class="fa fa-trash"></i>

                    </button>
                </form>
            </td>

            <td><img src="{{ $merchandising->image }}" width="40"></td>
            <td style="color: white"> {{ $merchandising->name }}</td>
            <td style="color: white"> {{ $merchandising->category->name }} </td>
            <td style="color: white"> {{ $merchandising->description }}</td>
            <td style="color: white"> {{ number_format($merchandising->price,2) }}</td>
            <td style="color: white"> {{ $merchandising->visible == 1 ?"SI" : "NO" }}</td>
            </tbody>
            @endforeach
        </table>

        <!-- Boton Nueva Categoria-->
        <div align="center">
            <a href="{{ route('admin.merchandising.create') }}" class="btn btn-warning"><i class="fa fa-plus-circle"></i>Añadir Nuevo Merchandising</a>
        </div>
    </div>

    <div>
    </div><br>


</div>

@stop
