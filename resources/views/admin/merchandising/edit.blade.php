@extends('admin.template')
@section('content')

<div style="background: #3B3A3A"><br>

    <h2 style="text-align: center"> - Editar Merchandising - </h2>
    <hr class="featurette-divider">

    <br><div class="table-responsive col-md-6 " style="width: 80%; margin:0 auto; background: white; border-radius: 3em; margin-bottom:  30px; text-align: center; color: black">

        @if(count($errors)> 0)
        @include('admin.partials.errors')
        @endif


        <form method="post" action="{{route('admin.merchandising.update',$merchandising->name)}}" enctype="multipart/form-data">
            @csrf <!-- {{ csrf_field() }} -->


            <label for="name">Nuevo Nombre</label><input type="text" name="name" class="form-control" placeholder="ingresa el nombre..." autofocus required>

            <label for="decription">Descripcion</label><input type="text" name="description" required class="form-control">

            <label for="price">Precio</label> <input type="text" name="price" required class="form-control">

            <label for="category_id">Categoria</label>
            <select name="category_id" class="form-control" required>
                @foreach($categories as $category)
                <option value="{{key($categories)}}">{{$category}}</option>
                @endforeach
            </select>

            <label for="visible">Visible</label><input type="checkbox" name="visible" class="form-control" required>

            <!-- <label for="image">Imagen</label><div class="fileupload-wrapper"><input type="file" id="input-file-now" class="mdb_upload" name="image"/> </div>

            <label for="audio">Audio</label><div class="fileupload-wrapper"><input type="file" id="input-file-now" class="mdb_upload" name="audio"/> </div> -->



            <label for="image">Imagen</label> <input type="text" name="image" class="form-control" required><br>


            <div>
                <input type="submit" class="btn btn-primary" value="Guardar">
                <a href="{{route('admin.merchandising.index')}}" class="btn btn-warning"> Cancelar</a>

            </div>
        </form>

    </div><br>
    @stop
</div>


