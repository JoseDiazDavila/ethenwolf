@extends('store.template')
@section('content')

<div style="background: #3B3A3A"><br>

    <h1 style="text-align: center; color: #F6E8AE"> - Detalle del Producto  - </h1>
    <hr class="featurette-divider">

    <br><div class="table-responsive" style="width: 80%; margin:0 auto; background: black; border-radius: 3em; margin-bottom:  30px; text-align: center; color:black">
        <table class="table">
            <thead>
                <tr><br>

            <img src="{{ $merchandising->image }}" width='450px'>

            <h3 style="color: white">Titulo del Producto : {{ $merchandising->name }}</h3>

            <p style="color: white">Detalles: {{$merchandising->description}}</p>
            <p style="color: white">Precio: {{$merchandising->price}} €</p><br>


            <hr class="featurette-divider">

            <a type="button" class="btn btn-border" href="{{ route('merchandising') }}" style="color: red">Volver Atrás </a>

            <a type="button" class="btn btn-border" href="{{ route('cart-add-merch', $merchandising->name) }}" style="color: red">Añadir al Carrito</a>

            </tr>
            </thead>
        </table>
    </div><br>
</div>
@stop