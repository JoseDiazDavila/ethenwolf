@extends('store.template')
@section('content')


<!-- fondo de pantalla -->
<div style="background: #3B3A3A"><br>

    <h1 style="text-align: center; color: #F6E8AE"> - Quienes Somos - </h1>
    <hr class="featurette-divider">

    <div class="container">

        <div class="col-md-12 col-md-offset-1">
            <h1 style="text-align: center; color: #F6E8AE">Información Sobre la Banda</h1>
            <hr class="featurette-divider">
            <h3>
                Ethenwolf es una banda española de Death Metal Melodico, que se formó en 2009.

                Son reconocidos por sus letras dedicadas frecuentemente a temas épicos y mitológicos, así como leyendas y cuentos macabros.

                Empezaron con Ivanov a la guitarra y el batería Reinhardt. Más tarde se les une Lucian como vocalista. 

                De esta manera nace la banda, mas tarde se les unen Sirk como bajista e Hypnos con los teclados, dando por finalizada la formación.</h3>

        </div><br>
        <hr class="featurette-divider"><br>



        <!-- COLUMNAS--> 
        <div class="row centro">
            <div class="col-md-12">

                <h2 style="color: #F6E8AE">Integrantes de Ethenwolf</h2> 
                <table class="table">
                    <tr>
                        <td><img class="img-responsive" src="images/fotos/conocenos/lucian.jpg" width="180px;" height="245px;"></td>
                        <td style="color: white;"><p>Lucian Vladcourt
                            <hr class="featurette-divider">
                            . Vocalista / Escritor de letras de Ethenwolf<br>
                            . Nacionalidad: Española<br>
                            . Fecha nacimiento: 17/12/1987
                            </p>
                        </td> 
                    </tr>
                    <tr>
                        <td><img  class="img-responsive" src="images/fotos/conocenos/adriael.jpg"width="180px;" height="245px;"></td>
                        <td style="color: white">
                            <p>Ivanov Adriael
                            <hr class="featurette-divider">
                            . Guitarrista / Compositor de melodias de Ethenwolf<br>
                            . Nacionalidad: Española<br>
                            . Fecha nacimiento: 13/02/1986
                            </p>
                        </td> 
                    </tr>
                    <tr>
                        <td><img  class="img-responsive" src="images/fotos/conocenos/sirk.jpg" width="180px;" height="245px;"></td>
                        <td style="color: white">
                            <p>Sirk
                            <hr class="featurette-divider">
                            . Bajista / Relaciones publicas  de Ethenwolf<br>
                            . Nacionalidad: Española<br>
                            . Fecha nacimiento: 09/08/1989
                            </p>
                        </td> 
                    </tr>
                    <tr>
                        <td><img  class="img-responsive" src="images/fotos/conocenos/hyp.jpg" width="180px;" height="245px;"></td>
                        <td style="color: white">
                            <p>Hypnos
                            <hr class="featurette-divider">
                            . Teclista / Editor de sonido de Ethenwolf<br>
                            . Nacionalidad: Española<br>
                            . Fecha nacimiento: 02/12/1995
                            </p>
                        </td> 
                    </tr>
                    <tr>
                        <td><img  class="img-responsive" src="images/fotos/conocenos/rein.jpg" width="180px;" height="245px;"></td>
                        <td style="color: white">
                            <p>Reinhardt
                            <hr class="featurette-divider">
                            . Baterista de Ethenwolf<br>
                            . Nacionalidad: Española<br>
                            . Fecha nacimiento: 07/05/1986
                            </p>
                        </td> 
                    </tr>
                </table>

                <hr class="featurette-divider"><br>



            </div>
        </div>


    </div>


</div>
@stop