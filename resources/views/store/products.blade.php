@extends('store.template')
@section('content')


<div style="background: #3B3A3A"><br>
    <h1 style="text-align: center; color: #F6E8AE"> - Nuestras Canciones - </h1>
    <hr class="featurette-divider">

    <div class="container " style="background: darkgrey"><br>
        <h2 style="color:  #F6E8AE; text-align: center"> - Álbum - The Breath Of The Wolf - </h2>

        <hr class="featurette-div#F6E8AEider"><br>
        <div class="row">
            @foreach($products AS $product)


            <div class="table-responsive col-sm-3 center" style=" margin-left: 6%; background: black; border-radius: 3em; margin-bottom:  30px; text-align: center; color: white">
                <table class="table">


                    <h3 style="font-size: 20px; color: darkred">{{$product->name}}</h3>
                    <img src="{{$product->image}}" width='250px'>
                    <p>{{$product->extract}}</p>
                    <p>{{$product->price}} €</p>

                    <a type="button" class="btn btn-border" href="{{ route('detail', $product->name) }} " style="color: red">Leer Más</a>
                    <a type="button" class="btn btn-border" href="{{ route('cart-add', $product->name)}}" style="color: red">Añadir Carrito</a>


                </table>

            </div>
            <br>
            @endforeach
        </div>
    </div><br>
</div>
@stop