@extends('store.template')
@section('content')

<div style="background: #3B3A3A"><br>



    <h2 style="text-align: center; color: #F6E8AE"> - Detalle del Pedido - </h2>

    <hr class="featurette-divider">

    <div class="container text-center">
        <div class="page">

            <div class="table-responsive">
                <h3 style="color: #F6E8AE">Datos del Usuario</h3>

                <table class="table table-striped  table-bordered" style="color: white">
                    <tr><td style="color: #F6E8AE">Nombre:</td><td>{{ Auth::user()->name . " " . Auth::user()->last_name }}</td> </tr>  
                    <tr><td style="color: #F6E8AE">Correo:</td><td>{{ Auth::user()->email }}</td> </tr>   

                </table>
            </div>

            <div class="table-responsive">
                <h3 style="color: #F6E8AE">Datos del Pedido</h3>

                <table class="table table-striped  table-bordered" style="color: white">
                    <tr>
                        <th style="color: #F6E8AE">Producto</th>
                        <th style="color: #F6E8AE">Precio</th>
                        <th style="color: #F6E8AE">Cantidad</th>
                    </tr>

                    @foreach($cart as $item)
                    <tr>
                        <td>{{ $item->name }}</td>
                        <td>{{ number_format($item->price, 2) }} €</td>
                        <td>{{ $item->quantity }}</td>
                    </tr>

                    @endforeach

                </table>

                <hr>

                <h2>

                    <span class="label label-success">
                        Total: {{ number_format($total, 2) }} €
                    </span>

                </h2><hr>
                <p>
                    <a href="{{ route('cart-show') }}" class="btn btn-primary">
                        <i class="fa fa-chevron-circle-left"></i> Volver Atras
                    </a>

                    <a href="{{ route('payment') }}" class="btn btn-warning">
                        Pagar con Paypal <i class="fa fa-cc-paypal fa-2x"></i>
                    </a>

                </p><br>

            </div>
        </div>
    </div>
</div>
@stop