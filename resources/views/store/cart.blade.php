@extends('store.template')
@section('content')

<div style="background: #3B3A3A"><br>

    <h1 style="text-align: center; color: #F6E8AE" > - Mi Carrito - </h1>
    <hr class="featurette-divider">

    <div  style=" width: 70%; margin: 0 auto; background: darkgray; border-radius: 2em"><br>

        <div class="container table-responsive">
            <table class="table table-striped table-bordered table-hover" >

                <!-- control por si no hay ada dentro de la cesta -->
                @if($cart)
                @if(count($cart))

                <thead style="text-align: center">
                    <tr>
                        <!-- Tabla para el carrito -->
                        <th style="color: #F6E8AE">Imagen</th>
                        <th style="color: #F6E8AE">Nombre del Producto</th>
                        <th style="color: #F6E8AE">Descripción</th>
                        <th style="color: #F6E8AE">Precio</th>
                        <th style="color: #F6E8AE">Cantidad</th>
                        <th style="color: #F6E8AE">Importe destinado a la donación</th>
                        <th style="color: #F6E8AE">Quitar del Carrito </th>
                    </tr>
                </thead>

                <tbody style="text-align: center; font-size: 15px">
                    <!-- bucle para recorrer los diferentes items -->
                    @foreach($cart as $item)
                    <tr>
                        <td> <img src="{{ $item->image }}" style="width: 80px"></td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->description }}</td>
                        <td>{{ number_format($item->price, 2) }}€</td>
                        <td> {{ $item->quantity }} </td>
                        <td>{{ number_format(($item->price * 10)/100, 2) }}€</td>
                        <td>
                            <a href=" {{ route('cart-delete', $item->name) }}" class="btn btn-danger"><i class="fa fa-remove"></i></a>
                        </td>
                    </tr> 
                    @endforeach
                </tbody>

                <tfoot>
                    <tr>
                        <!-- Donacion total -->
                        <th style="color: #F6E8AE; font-size:20px" colspan="5">Donación Total:</th>
                        <th colspan="2" style="text-align:center"> 
                            <h3>
                                <span class="label label-success" >{{ number_format($total * 10/100, 2) }} €</span>
                            </h3>
                        </th>
                    </tr>
                    <tr>
                        <!-- Cantidad final -->
                        <th style="color: #F6E8AE; font-size:20px" colspan="5">Precio Final:</th>
                        <th colspan="2" style="text-align:center"> 
                            <h3>
                                <span class="label label-success" >{{ number_format($total, 2) }} €</span>
                            </h3>
                        </th>
                    </tr>
                </tfoot>
                <!-- Si no hay nada en la cesta salta mensaje-->

                @endif
                @else
                <h3 style="text-align: center"><span class="label label-warning">Carrito Vacio</span></h3>
                @endif
            </table>
        </div>

        <hr class="featurette-divider">

        <h3 style="text-align:center">

            <!-- boton seguir comprando -->
            <a class="btn btn-primary" href="{{ route('inicio') }}" style=" text-align: center; color: black"><i class="fa fa-chevron-circle-left"></i>Seguir Comprando</a>

            <!-- boton pagar -->
            <a class="btn btn-success" href="{{ route('order-detail') }}" style="text-align: center; color: black">Continuar y ver detalle del pedido<i class="fa fa-chevron-circle-right"></i></a>
        </h3>

        <h3 style="text-align:center">

            <!-- boton vaciar cesta -->
            <a style="text-align: center" href="{{ route('cart-trash') }}" class="btn btn-danger"><i class="fa fa-trash"></i> Vaciar Cesta </a>
        </h3><br>

    </div>
    <br>
</div>
@stop