@extends('store.template')
@section('content')

<div style="background: #3B3A3A"><br>

    <h2 style="text-align: center; color: #F6E8AE"> - Contacta con nosotros - </h2>
    <hr class="featurette-divider">

    <br><div class="table-responsive col-md-6 " style="width: 80%; margin:0 auto; background: grey; border-radius: 3em; margin-bottom:  30px; text-align: center; color: black">

        <!-- Formulario -->
        <form role="form" method="POST" action="{{route('contactanos')}}">
           @csrf
            <br> <div class="form-group" style="color: white">
                <label for="nombre">Tu Nombre</label>
                <input name="nombre" type="text" class="form-control" id="ejemplo_password_1" placeholder="Tu nombre">
            </div>

            <div class="form-group" style="color: white">
                <label for="ejemplo_email_1">Tu Email</label>
                <input name="email" type="email" class="form-control" id="ejemplo_email_1" placeholder="Introduce tu email">
            </div>

            <div class="form-group" style="color: white">
                <label for="message">Asunto</label>
                <input name="asunto" type="text" class="form-control md-textarea" placeholder="Asunto"></input><br>
            </div>
            

            <div class="md-form" style="color: white">
                <label for="message">Tu mensaje</label>
                <textarea name="content" type="text" cols="30" rows="8" class="form-control md-textarea" placeholder="Tu mensaje"></textarea><br>
            </div>

            <!-- Botones -->
            <div class="form-group" style="color: white">
                <label for="ejemplo_archivo_1">Adjuntar un archivo</label>
                <input name="file" type="file" id="ejemplo_archivo_1">
            </div>

            <button type="submit" class="btn btn-default" >Enviar</button>
        </form><br>

    </div><br>
</div>
@stop

