@extends('store.template')
@section('content')

<!-- fondo de pantalla -->
<div style="background: #3B3A3A"><br>
    <h1 style="text-align: center; color: #F6E8AE"> - Merchandising - </h1>
    <hr class="featurette-divider">

    <div class="container " style="background: darkgrey"><br>
        <div class="row">

            @foreach($merchandising AS $merchandising)

            <div class="table-responsive col-sm-4 center" style=" margin-left: 12%; background: black; border-radius: 3em; margin-bottom:  30px; text-align: center; color: white">
                <table class="table">

                    <h3 style="font-size: 20px ; color: darkred">{{$merchandising->name}}</h3>
                    <img src="{{$merchandising->image}}" width='200px'>
                    <p>{{$merchandising->description}}</p>
                    <p>{{$merchandising->price}} €</p>

                    <a  type="button" class="btn btn-border" href="{{ route('detail-merch', $merchandising->name) }} " style="color: red">Leer Más</a>
                    <a  type="button" class="btn btn-border" href="{{ route('cart-add-merch', $merchandising->name)}}" style="color: red">Añadir Carrito</a>

                </table>

            </div><br>

            @endforeach
        </div>
    </div><br>
</div>
@stop