
@if(Auth::check())

<li class="dropdown" style="list-style: none">

    <a href="#" type="button" class="dropdown-toggle btn-border" style="color: #A5A5A5" data-toggle="dropdown" role="button" aria-expanded="false">
        <i class="fa fa-user fa-2x"></i>{{ Auth::user()->user }}<span class="caret"></span>
    </a>

    <ul class="dropdown-menu" role="menu"  aria-labelledby="dLabel">
        <li>
            <a class="btn btn-link" style="color: #A5A5A5" href="{{route('logout')}}"> Finalizar Sesión</a>
        </li>
    </ul>
</li>

@else

<li class="dropdown" style="list-style: none">

    <a href="#" type="button" class="dropdown-toggle btn-border" data-toggle="dropdown" role="button" aria-expanded="false">
        <i class="fa fa-user fa-2x"></i><span class="caret"></span>
    </a>

    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
        <li>
            <a class="btn btn-link" style="color: #A5A5A5" href="{{route('login')}}"> Iniciar Sesión</a>
        </li>
        <li>
            <a class="btn btn-link" style="color: #A5A5A5" href="{{route('register')}}"> Registrarse</a>
        </li>
    </ul>
</li>

@endif