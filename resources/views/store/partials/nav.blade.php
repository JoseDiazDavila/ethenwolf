
<div class="page-header section-dark" style="background-image: url('images/fotos/fondos/fondo1.jpg');" >
    <div class="filter"></div>
    <div class="content-center">
        <div class="container">
            <div class="title-brand">

                <!-- Nombre del sitio -->
                <h1 class="presentation-title">Ethenwolf</h1>
                <!-- Subtitulo del sitio -->
                <h4>The Official Website</h4>

                <div class="fog-low"> <img src="./assets/img/fog-low.png" alt=""> </div>
                <div class="fog-low right"> <img src="./assets/img/fog-low.png" alt=""> </div>
            </div>

        </div>
    </div>
    <div class="moving-clouds" style="background-image: url('/images/fotos/logos/clouds.png') "></div>
</div>



<!-- Navegador-->
<nav class="navbar navbar-expand-lg  navbar-light justify-content-md-center " style="background: black " >

    <!-- para ocultar los puntos del LI -->
    <li class="dropdown" style="list-style: none" >

        <!-- boton inicio de sesion-->
        <button type="button" class="btn btn-border" ><a class="nav-item" href="#">   @include('store.partials.menu-user')  </a></button>

        <!-- Condicion para administrador-->
        @if(Auth::check() && Auth::user()->type == 'admin')
        <a type="button" class="nav-item btn btn-border btn-danger" style="color: #A5A5A5" href="admin/home"> ADMINISTRADOR</a>
        @endif

        <a type="button" class="nav-item btn btn-border" style="color: #A5A5A5" href="{{ route('inicio') }}" >Inicio</a>
        <a type="button" class="nav-item btn btn-border" style="color: #A5A5A5" href="{{ route('quienesSomos') }}">Quienes Somos</a>
        <a type="button" class="nav-item btn btn-border" style="color: #A5A5A5" href="{{ route('producto') }}">Canciones</a>
        <a type="button" class="nav-item btn btn-border" style="color: #A5A5A5" href="{{ route('merchandising') }}">Merchandising</a>
        <a type="button" class="nav-item btn btn-border" style="color: #A5A5A5" href="{{ route('asociaciones') }}">Asociaciones </a>
        <a type="button" class="nav-item btn btn-border" style="color: #A5A5A5" href="{{ route('conciertos') }}">Videos de Conciertos</a>
        <a type="button" class="nav-item btn btn-border" style="color: #A5A5A5" href="{{ route('contactanos') }}">Contáctanos</a></button>
        <a type="button" class="nav-item btn btn-border" style="color: #A5A5A5" href=" {{ route('cart-show') }}"><i class="fa fa-shopping-basket fa-2x"></i></a>

    </li>
</nav>

