<footer class="footer ">
    <div class="container">
        <div class="row">
            <nav class="footer-nav">
                <ul>
                    <li>
                        <!-- Icono redireccion facebook -->

                        <a href="https://www.facebook.com/pages/category/Musician-Band/Ethenwolf-192756314149998/" target="_blank"> <i style="font-size:24px" class="fa">&#xf082;</i><br>FaceBook</a>

                    </li>
                    <li>
                        <!-- Icono redireccion youtube (NO FUNCIONA EL ICONO, SOLO EL NOMBRE) -->

                        <a href="https://www.youtube.com/user/EthenwolfOfficial" target="_blank"><i style="font-size:24px" class="fa">&#xf166;</i><br>YouTube</a>
                    </li>
                </ul>
            </nav>

            <div class="credits ml-auto">
                <!-- Copyright -->
                <span class="copyright"> © <script> document.write(new Date().getFullYear());</script>, made with by José Manuel Díaz Dávila</span>
            </div>

        </div>
    </div>
</footer>