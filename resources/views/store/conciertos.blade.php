@extends('store.template')
@section('content')


<!-- fondo de pantalla -->
<div style="background: #3B3A3A"><br>

    <h1 style="text-align: center; color: #F6E8AE"> - Videos de Nuestros Conciertos - </h1>
    <hr class="featurette-divider">

    <div  style=" width: 80%; margin: 0 auto; background: darkgray; border-radius: 1%"><br>

        <div style="width: 80%; margin:0 auto; background: black; border-radius: 3em; margin-bottom:  30px; text-align: center; color: white">
            <h1>The Black Cat</h1> 
            <iframe width="900" height="500" src="https://www.youtube.com/embed/BsjF3f9Cg-8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>

        <div style="width: 80%; margin:0 auto; background: black; border-radius: 3em; margin-bottom:  30px; text-align: center; color: white">
            <h1>The Land Of The Valkyries</h1> 
            <iframe width="900" height="500" src="https://www.youtube.com/embed/CWJtI6utlGA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>

        <div style="width: 80%; margin:0 auto; background: black; border-radius: 3em; margin-bottom:  30px; text-align: center; color: white">
            <h1>Shadow Melodies</h1> 
            <iframe width="900" height="500" src="https://www.youtube.com/embed/I2RabAs6JR4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div><br>
    </div><br>

</div>
@stop