@extends('store.template')
@section('content')

<!-- fondo de pantalla -->
<div style="background: #3B3A3A">

    <!-- contenedor-->
    <div class="container"><br>

        <!--carousel-->
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="width: 100%; ">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="1" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
            </ol>

            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="images/fotos/slider/slider1.jpg" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="images/fotos/slider/slider2.jpg" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="images/fotos/slider/slider3.jpg" alt="Third slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="images/fotos/slider/slider4.jpg" alt="Fourth slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="images/fotos/slider/slider5.jpg" alt="Fifth slide">
                </div>
            </div>

            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <div>
            <!-- Informacion -->
            <h1 style="color: #F6E8AE">Información</h1>
            <hr class="featurette-divider">
            <p class="lead">Las compras que realices en esta pagina, tendrán un porcentaje de descuento 
                que ira destinado a ayudar a distintas Asociaciones benéficas, podrás encontrar mas información 
                en nuestro apartado de <a href="{{ route('asociaciones') }}">Asociaciones Benéficas</a>.</p>
        </div>
        <div class="container marketing">

            <!-- Noticias -->
            <h1 style="color: #F6E8AE">Noticias</h1>

            <hr class="featurette-divider">

            <div class="row featurette">
                <div class="col-md-7">
                    <h2 class="featurette-heading" style="color: #F6E8AE">Festival noche de Halloween - Ethenwolf - Darkblazers - Vestigia - Judas Resurrection </h2>
                    <h3 class="featurette-heading">Localicacion y Fecha: Sala Caracol - (31/10) </h3>
                    <hr class="featurette-divider">
                    <p class="lead">¡¡Aquí podrás  ver una critica de Zombie War, sobre el Festival de la noche de Halloween!!</p>
                    <p class="lead">
                        <a href="https://zombiewarmanagement.com/concierto-de-noche-de-halloween-ethenwolf-darkblazers-vestigia-judas-resurrection/">Ver Artículo de Zombie War</a>
                    </p>
                </div>
                <div class="col-md-5">
                    <img class="featurette-image img-fluid mx-auto"   data-holder-rendered="true" style="width: 500px; height: 600px;" src="https://zombiewarmanagement74.files.wordpress.com/2012/09/8a230-fiestahalloween.png" >
                </div>
            </div>

            <hr class="featurette-divider">

            <div class="row featurette">
                <div class="col-md-7 order-md-2">
                    <h2 class="featurette-heading" style="color: #F6E8AE">Concierto - Vanagloria - Ethenwolf - Necroberus </h2>
                    <h3 class="featurette-heading">Localicacion y Fecha: Sala We Rock  - (09/03)</h3>
                    <hr class="featurette-divider">
                    <p class="lead">Aquí podrás ver más información, sobre nuestro próximo concierto junto a otros grandes grupos, en la mítica sala We Rock!, ¡No te lo pierdas!</p>
                    <p class="lead">
                        <a href="http://www.theconcertinconcert.com/vanagloriaethenwolfnecroberus-el-9-de-marzo-en-we-rock/">Ver Más Información</a>
                    </p>

                </div>
                <div class="col-md-5 order-md-1">
                    <img class="featurette-image img-fluid mx-auto"  data-holder-rendered="true" style="width: 500px; height: 600px;" src="http://www.theconcertinconcert.com/wp-content/uploads/2013/01/VANAGLORIA+ETHENWOLF+NECROBERUS-090313-WE-ROCK.jpg" >
                </div>
            </div>

            <hr class="featurette-divider">

            <div class="row featurette">
                <div class="col-md-7">
                    <h2 class="featurette-heading" style="color: #F6E8AE">¡¡Nuestra Maqueta!! </h2>
                    <h3 class="featurette-heading"> Ethenwolf - The Breath Of The Wolf </h3>
                    <hr class="featurette-divider">
                    <p class="lead">¡¡Ya puedes comprar nuestra albúm al completo en el siguiente enlace!!
                        <a href="{{ route('producto') }}">Canciones</a>
                        Recuerda que un porcentaje de tu compra irá destinado a una asociacion benefica.</p>
                </div>
                <div class="col-md-5">
                    <img class="featurette-image img-fluid mx-auto"  data-holder-rendered="true" style="width: 450px; height: 450px;" src="images/fotos/portadas/portada.jpg">
                </div>
            </div>

            <hr class="featurette-divider">

            <!-- /END THE FEATURETTES -->

        </div>

        <!-- Comentarios -->
        <div class="row">

            <div class="col-lg-4">
                <img class="rounded-circle" alt="Generic placeholder image" width="140" height="140" src="images/fotos/galeria/men1.jpg">
                <h2>R. Cleremont</h2>
                <p>Una banda nacional que merece la pena sacar hacia adelante.</p>
                <h5 style="color: #F6E8AE">"Rolling Stone - Magazine"</h5>
            </div>

            <div class="col-lg-4">
                <img class="rounded-circle" alt="Generic placeholder image" width="140" height="140" src="images/fotos/galeria/girl1.jpg">
                <h2>Elisa Mazza</h2>
                <p>Desafiantes a la escena musical actual, muy novedosos y cargados de fuerza.</p>
                <h5 style="color: #F6E8AE">"Kerrang!"</h5>
            </div>

            <div class="col-lg-4">
                <img class="rounded-circle" alt="Generic placeholder image" width="140" height="140" src="images/fotos/galeria/men3.jpg">
                <h2>Lepunk</h2>
                <p>No es mi estilo y aun así, me encantan.</p>
                <h5 style="color: #F6E8AE"> "Punk - Magazine"</h5>
            </div>

        </div>
        <hr class="featurette-divider"><br>


    </div>
</div>
@stop
