<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoryTableSeeder extends Seeder {

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {

        $data = array(
            [
                'name' => 'Canciones',
                'description' => 'Apartado de venta de Canciones'],
              [
                'name' => 'Merchandising',
                'description' => 'Apartado de venta de Merchandising']
        );

        Category::insert($data);
    }

}
