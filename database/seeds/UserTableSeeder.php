<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = array(
            [
                'name' => 'Jose',
                'last_name' => 'Diaz',
                'email' => 'foxhaund15@hotmail.com',
                'user' => 'Lucian',
                'password' => Hash::make('123456'),
                'type' => 'admin',
                'active' => 1,
                'address' => 'Calle Falsa 1234, Getafe, Madrid',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'name' => 'Ivan',
                'last_name' => 'Lorente',
                'email' => 'adriael@hotmail.com',
                'user' => 'Adriael',
                'password' => Hash::make('123456'),
                'type' => 'user',
                'active' => 1,
                'address' => 'Calle Super Falsa 1234, Getafe, Madrid',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
        );
        User::insert($data);
    }

}
