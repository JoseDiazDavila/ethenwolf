<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductTableSeeder extends Seeder {

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {

        $data = array(
            [
                'name' => 'Inmortal Knight',
                'description' => 'Tack - 01 Inmortal Knight - Disco: The Breath Of The Wolf - Fecha de grabacion: 06/06/2012',
                'extract' => '01 - Inmortal Knight',
                'price' => 0.20,
                'image' => '../images/fotos/portadas/portada.jpg',
                'visible' => 1,
                'audio' => '../images/descargable/audios/INMORTAL_KNIGHT.mp3',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 1
            ],
            [
                'name' => 'Lady Mesmerized',
                'description' => 'Tack - 02 Lady Mesmerized - Disco: The Breath Of The Wolf - Fecha de grabacion: 06/06/2012',
                'extract' => '02 - Lady Mesmerized',
                'price' => 0.20,
                'image' => '../images/fotos/portadas/portada.jpg',
                'visible' => 1,
                'audio' => '../images/descargable/audios/LADY_MESMERIZED.mp3',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 1
            ],
            [
                'name' => 'Land of the Valkiries',
                'description' => 'Tack - 03 Land of the Valkiries - Disco: The Breath Of The Wolf - Fecha de grabacion: 06/06/2012',
                'extract' => '03 - Land of the Valkiries',
                'price' => 0.20,
                'image' => '../images/fotos/portadas/portada.jpg',
                'visible' => 1,
                'audio' => '../images/descargable/audios/Land_of_The_Valkyries.mp3',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 1
            ],
            [
                'name' => 'Requiem',
                'description' => 'Tack - 04 Requiem - Disco: The Breath Of The Wolf - Fecha de grabacion: 06/06/2012',
                'extract' => '04 - Requiem',
                'price' => 0.20,
                'image' => '../images/fotos/portadas/portada.jpg',
                'visible' => 1,
                'audio' => '../images/descargable/audios/REQUIEM.mp3',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 1
            ],
            [
                'name' => 'Shadow Melodies',
                'description' => 'Tack - 05 Shadow Melodies - Disco: The Breath Of The Wolf - Fecha de grabacion: 06/06/2012',
                'extract' => '05 - Shadow Melodies',
                'price' => 0.20,
                'image' => '../images/fotos/portadas/portada.jpg',
                'visible' => 1,
                'audio' => '../images/descargable/audios/SHADOW_MELODIES.mp3',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 1
            ],
            [
                'name' => 'The sound of the blood rain',
                'description' => 'Tack - 06 The sound of the blood raint - Disco: The Breath Of The Wolf - Fecha de grabacion: 06/06/2012',
                'extract' => '06 - The sound of the blood rain',
                'price' => 0.20,
                'image' => '../images/fotos/portadas/portada.jpg',
                'visible' => 1,
                'audio' => '../images/descargable/audios/THE_SOUND_OF_THE_BLOOD_RAIN.mp3',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 1
            ]
        );

        Product::insert($data);
    }

}
