<?php

use Illuminate\Database\Seeder;
use App\Merchandising;

class MerchandisingTableSeeder extends Seeder {

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {

        $data = array(
            [
                'name' => 'Sudadera Ethenwolf - Chico',
                'description' => 'Tallas Disponibles: S / M / L / XL',
                'price' => 20,
                'image' => '../images/fotos/merchandising/sudadera01.png',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 1
            ],
            [
                'name' => 'Sudadera Ethenwolf - Chica',
                'description' => 'Tallas Disponibles: S / M / L / XL',
                'price' => 20,
                'image' => '../images/fotos/merchandising/sudadera02.png',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 1
            ],
            [
                'name' => 'Camiseta Ethenwolf - Chico',
                'description' => 'Tallas Disponibles: S / M / L / XL',
                'price' => 12,
                'image' => '../images/fotos/merchandising/camiseta01.png',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 1
            ],
            [
                'name' => 'Camiseta Ethenwolf - Chica',
                'description' => 'Tallas Disponibles: S / M / L / XL',
                'price' => 12,
                'image' => '../images/fotos/merchandising/camiseta02.png',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 1
            ],
            [
                'name' => 'Chapa Ethenwolf - Modelo 01',
                'description' => '',
                'price' => 2,
                'image' => '../images/fotos/merchandising/chapa01.png',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 1
            ],
            [
                'name' => 'Chapa Ethenwolf - Modelo 02',
                'description' => '',
                'price' => 2,
                'image' => '../images/fotos/merchandising/chapa02.png',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 1
            ],
            [
                'name' => 'Llavero Ethenwolf - Modelo 01',
                'description' => '',
                'price' => 3,
                'image' => '../images/fotos/merchandising/llavero01.png',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 1
            ],
            [
                'name' => 'Llavero Ethenwolf - Modelo 02',
                'description' => '',
                'price' => 3,
                'image' => '../images/fotos/merchandising/llavero02.png',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 1
            ]
        );

        Merchandising::insert($data);
    }

}
