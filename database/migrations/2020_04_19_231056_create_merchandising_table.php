<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMerchandisingTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('merchandising', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name', 255);
            $table->text('description');
            $table->decimal('price', 5, 2);
            $table->string('image', 300);
            $table->boolean('visible');
            /* definir clave foranea */
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')
                    ->references('id')
                    ->on('merchandising')
                    ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('merchandising');
    }

}
