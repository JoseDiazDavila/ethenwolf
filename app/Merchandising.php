<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Merchandising extends Model {

    protected $table = 'merchandising';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'price', 'image', 'visible', 'category_id'];

    /* relacion con category */

    public function category() {
        return $this->belongsTo('App\Category');
    }

    public function order_item() {
        return $this->hasOne('App\OrderItem');
    }

}
