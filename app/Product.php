<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

    protected $table = 'products';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'extract', 'price', 'image', 'visible', 'audio', 'category_id'];

    /* relacion con category */

    public function category() {
        return $this->belongsTo('App\Category');
    }

    public function order_item() {
        return $this->hasOne('App\OrderItem');
    }

}
