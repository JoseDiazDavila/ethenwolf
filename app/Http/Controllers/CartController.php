<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Merchandising;

class CartController extends Controller {

    public function _contruct() {
        if (!\Session::has('cart'))
            \Session::put('cart', array());
    }

    /* vista carrito */

    public function show() {
        $cart = \Session::get('cart');
        $total = $this->total();

        return view('store.cart', compact('cart', 'total'));
    }

    /* añadir cancion al carrito */

    public function add(Product $product) {
        $cart = \Session::get('cart');
        $product->quantity = 1;
        $cart[$product->name] = $product;
        \Session::put('cart', $cart);
        return redirect()->route('cart-show');
    }

    /* borrar producto del carrito */

    public function delete(Product $product) {
        $cart = \Session::get('cart');
        unset($cart[$product->name]);
        \Session::put('cart', $cart);
        return redirect()->route('cart-show');
    }

    /* actualizar cantidad canciones carrito */

    public function update(Product $product, $quantity) {
        $cart = \Session::get('cart');
        $cart[$product->name]->quantity = $quantity;
        \Session::put('cart', $cart);

        return redirect()->route('show');
    }

    /* añadir merch al carrito */

    public function add_merch(Merchandising $merchandising) {
        $cart = \Session::get('cart');
        $merchandising->quantity = 1;
        $cart[$merchandising->name] = $merchandising;
        \Session::put('cart', $cart);
        return redirect()->route('cart-show');
    }

    /* borrar merch del carrito */

    public function delete_merch(Merchandising $merchandising) {
        $cart = \Session::get('cart');
        unset($cart[$merchandising->name]);
        \Session::put('cart', $cart);
        return redirect()->route('cart-show');
    }

    /* actualizar cantidad merch carrito */

    public function update_merch(Merchandising $merchandising, $quantity) {
        $cart = \Session::get('cart');
        $cart[$merchandising->name]->quantity = $quantity;
        \Session::put('cart', $cart);

        return redirect()->route('show');
    }

    /* vaciar canciones carrito */

    public function trash() {
        \Session::forget('cart');
        return redirect()->route('cart-show');
    }

    /* calcular total */

    public function total() {
        $cart = \Session::get('cart');
        $total = 0;
        if ($cart != null) {
            foreach ($cart as $item) {
                $total += $item->price * $item->quantity;
            }
        }
        return $total;
    }

    /* funcion redireccionar a detalla carrito si tenemos algo en el carrito */

    public function orderDetail() {

        if (count(\Session::get('cart')) <= 0)
            return redirect()->route('home');
        $cart = \Session::get('cart');
        $total = $this->total();
        return view('store.order-detail', compact('cart', 'total'));
    }

}
