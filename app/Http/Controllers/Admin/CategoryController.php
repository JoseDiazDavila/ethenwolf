<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller {
    /* recibir todo de categorias y guardarlo en la variable categories */

    public function index() {
        $categories = Category::all();
        //dd($categories);

        return view('admin.category.index', compact('categories'));
    }

    public function create() {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //return $request->all();
        $this->validate($request, [
            'name' => 'required|unique:categories|max:255',
            'description' => 'required'
        ]);
        $Category = Category::create([
                    'name' => $request->get('name'),
                    'description' => $request->get('description')
        ]);
        $message = $Category ? 'Categoria agregada correctamente' : 'No se ha podido agregar la categoria';

        return redirect()->route('admin.category.index')->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category) {
        return $category;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category) {
        return view('admin.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category) {
        $category->fill($request->all());

        $update = $category->save();

        $message = $update ? 'categoria actualizada correctamente' : 'No se ha podido actualizar la categoria';

        return redirect()->route('admin.category.index')->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category) {
        $deleted = $category->delete();
        $message = $deleted ? 'Categoria eliminada correctamente' : 'No se ha podido eliminar la categoria';
        return redirect()->route('admin.category.index')->with('message', $message);
    }

}
