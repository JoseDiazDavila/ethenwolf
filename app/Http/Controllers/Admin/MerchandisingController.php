<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Database\Eloquent;
use App\Http\Requests\SaveMerchandisingRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Merchandising;
use App\Category;

class MerchandisingController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
    $merchandising = Merchandising::all();
    return view('admin.merchandising.index', compact('merchandising'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $categories = Category::orderBy('id', 'desc')->pluck('name', 'id')->all();
        return view('admin.merchandising.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveMerchandisingRequest $request) {

        $data = [
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'price' => $request->get('price'),
            'image' => $request->get('image'),
            'visible' => $request->get('visible') ? 1 : 0,
            'category_id' => $request->get('category_id'),
        ];
        $merchandising = Merchandising::create($data);

        $message = $merchandising ? 'Merchandising agregado correctamente' : 'No se ha podido agregar el merchandising';

        return redirect()->route('admin.merchandising.index')->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Merchandising $merchandising) {
        return $merchandising;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Merchandising $merchandising) {

        $categories = Category::orderBy('id', 'desc')->pluck('name', 'id')->all();

        return view('admin.merchandising.edit', compact('categories', 'merchandising'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Merchandising $merchandising) {

        $merchandising->fill($request->all());

        $merchandising->visible = $request->has('visible') ? 1 : 0;
        $update = $merchandising->save();


        $message = $update ? 'Merchandising actualizado correctamente' : 'No se ha podido actualizar el merchandising';

        return redirect()->route('admin.merchandising.index')->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Merchandising $merchandising) {
        $deleted = $merchandising->delete();
        $message = $deleted ? 'Merchandising eliminado correctamente' : 'No se ha podido eliminar el merchandising';
        return redirect()->route('admin.merchandising.index')->with('message', $message);
    }

}
