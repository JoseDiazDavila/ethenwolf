<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Database\Eloquent;
use App\Http\Requests\SaveProductRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;

class ProductController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $products = Product::OrderBy('id', 'desc')->paginate(5);
        return view('admin.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $categories = Category::orderBy('id', 'desc')->pluck('name', 'id')->all();
        return view('admin.product.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveProductRequest $request) {

        $data = [
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'extract' => $request->get('extract'),
            'price' => $request->get('price'),
            'image' => $request->get('image'),
            'audio' => $request->get('audio'),
            'visible' => $request->get('visible') ? 1 : 0,
            'category_id' => $request->get('category_id'),
        ];
        $product = Product::create($data);

        $message = $product ? 'Producto agregado correctamente' : 'No se ha podido agregar el producto';

        return redirect()->route('admin.product.index')->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product) {
        return $product;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product) {

        $categories = Category::orderBy('id', 'desc')->pluck('name', 'id')->all();
        
        return view('admin.product.edit', compact('categories', 'product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product) {

        $product->fill($request->all());

        $product->visible = $request->has('visible') ? 1 : 0;
        $update = $product->save();
        

        $message = $update ? 'Producto actualizado correctamente' : 'No se ha podido actualizar el producto';

        return redirect()->route('admin.product.index')->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(Product $product)
    {
        $deleted = $product->delete();
        $message = $deleted ? 'Producto eliminado correctamente' : 'No se ha podido eliminar el producto';
        return redirect()->route('admin.product.index')->with('message',$message);
    }

}
