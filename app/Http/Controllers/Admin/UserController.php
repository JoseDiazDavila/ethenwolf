<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SaveUserRequest;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller {
    /* recibir todo de categorias y guardarlo en la variable categories */

    public function index() {
        $users = User::orderBy('name', 'desc')->paginate(5);
        return view('admin.user.index', compact('users'));
    }

    public function create() {
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveUserRequest $request) {
        $requestActive =  $request->get('active');
        $active = $requestActive =='on' ? 1 : 0;
        $data = [
            'name' => $request->get('name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'user' => $request->get('user'),
            'password' => $request->get('password'),
            'type' => $request->get('type'),
            'active' => $active,
            'address' => $request->get('address')
        ];
        $user = User::create($data);
        $message = $user ? 'Usuario agregado correctamente' : 'No se ha podido agregar al usuario';

        return redirect()->route('admin.user.index')->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user) {
        return $user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user) {
        return view('admin.user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SaveUserRequest $request, User $user) {
        $requestActive =  $request->get('active');
        $active = $requestActive =='on' ? 1 : 0;
        
        $user->name = $request->get('name');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        $user->password = ($request->get('password'));
        $user->password = $request->get('password');
        $user->type = $request->get('type');
        $user->user = $request->get('user');
        $user->active = $active;
        $user->address = $request->get('address');


        //$user->fill($request->all());
        $update = $user->save();

        $message = $update ? 'Usuario actualizado correctamente' : 'No se ha podido actualizar al usuario';

        return redirect()->route('admin.user.index')->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user) {
        $deleted = $user->delete();
        $message = $deleted ? 'Usuario eliminado correctamente' : 'No se ha podido eliminar al usuario';
        return redirect()->route('admin.user.index')->with('message', $message);
    }

}
