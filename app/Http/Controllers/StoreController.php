<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Merchandising;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactMail;

class StoreController extends Controller {

    //esta funcion retorna la vista del index
    public function index() {
        return view('store.index');
    }

    //esta funcion retorna la vista de las asociaciones
    public function asociaciones() {
        return view('store.asociaciones');
    }

    //esta funcion retorna la vista de los conciertos
    public function conciertos() {
        return view('store.conciertos');
    }

    //esta funcion retorna la vista de contactanos
    public function contactanos() {
        return view('store.contactanos');
    }
    
     public function email(Request $request){
       Mail::send(new ContactMail($request));
       return redirect('/')
       ->with('message','Su mensaje ha sido enviado correctamente.');
   }
    
    //esta funcion retorna la vista de quienes somos
    public function quienesSomos() {
        return view('store.quienesSomos');
    }


    //esta funcion retorna la vista de merchandising
    public function merchandising() {

        $merchandising = Merchandising::all();

        return view('store.merchandising', compact('merchandising'));
    }
    
     public function show_merch($name) {

        $merchandising = Merchandising::where('name', $name)->first();

        return view('store.show-merch', compact('merchandising'));
    }

    //esta funcion hace uan consulta que recoge todos los productos en una variable y enviamos la variable a la vista de productos
    public function product() {

        $products = Product::all();

        return view('store.products', compact('products'));
    }

    //esta funcion recoge una variable por parametros. hacemos una consulta donde el nombre sea igual al de la bdd y 
    //coincidan, lo metemos en una variable q va a la vista
    public function show($name) {

        $products = Product::where('name', $name)->first();

        return view('store.show', compact('products'));
    }

}
