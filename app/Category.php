<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

    protected $table = 'categories';
    protected $fillable = ['name', 'description'];
    public $timestamps = false;

    //Relacion con articulos
    public function products() {
        return $this->hasMany('App\Product');
    }

}
